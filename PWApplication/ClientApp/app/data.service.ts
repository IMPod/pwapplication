﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from './user';


@Injectable()
export class DataService {

    private url = "https://localhost:44391/api/Users";

    constructor(private http: HttpClient) {
    }

    getUsers() {
        let el = this.http.get(this.url);
        return el;
    }

    createUsers(users: User) {
        return this.http.post(this.url, users);
    }
    updateUsers(users: User) {

        return this.http.put(this.url + '/' + users.UserId, users);
    }
    deleteUsers(id: string) {
        console.log(id);
        return this.http.delete(this.url + '/' + id);
    }
}