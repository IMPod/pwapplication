﻿import { Component, OnInit } from '@angular/core';
import { DataService } from './data.service';
import { User } from './user';

@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    providers: [DataService]
})
export class AppComponent implements OnInit {

    user: User = new User();
    users: User[];
    tableMode: boolean = true;

    constructor(private dataService: DataService) { }

    ngOnInit() {
        this.loadUsers();
    }

    loadUsers() {
        this.dataService.getUsers()
            .subscribe((data: User[]) => this.users = data);
    }

    save() {
        if (this.user.UserId == null) {
            this.dataService.createUsers(this.user)
                .subscribe((data: User) => this.users.push(data));
        } else {
            this.dataService.updateUsers(this.user)
                .subscribe(() => this.loadUsers());
        }
        this.cancel();
    }

    editUser(p: User) {
        this.user = p;
    }

    cancel() {
        this.user = new User();
        this.tableMode = true;
    }

    delete(p: User) {
        this.dataService.deleteUsers(p.UserId)
            .subscribe(() => this.loadUsers());
    }

    add() {
        this.cancel();
        this.tableMode = false;
    }
}