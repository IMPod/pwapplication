﻿import { Injectable } from '@angular/core';
import { ValidationErrors } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
@Injectable()
export class EmailValidationService {
    private emails: string[];
    constructor() {
   
        this.emails = [];
    }

    validateName(userEmail: string): Observable<ValidationErrors> {

        return new Observable<ValidationErrors>(observer => {
            const email = this.emails.find(email => email === userEmail);
            if (email) {
                observer.next({
                    nameError: 'Email not unique'
                });
                observer.complete();
            }

            observer.next(null);
            observer.complete();
        });
    }
}