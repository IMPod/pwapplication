﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Const;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly PWApplicationContext _context;

        public UsersController(PWApplicationContext context)
        {
            this._context = context;
        }

        // GET: api/Users
        [HttpGet]
        [AllowAnonymous]
        public void OnGet()
        {           
            return;
        }

        // GET: api/Users
        [HttpGet]
       // [Authorize]
        public IEnumerable<User> Get()
        {
            var el = this._context.Users;
            return el;

        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var users = await this._context.Users.FindAsync(id);

            if (users == null)
            {
                ModelState.AddModelError("", "User not found");
                return NotFound(ModelState);
            }

            return Ok(users);
        }

        // PUT: api/Users/5
        [HttpPut("{id}")]
     //   [Authorize]
        public async Task<IActionResult> Put([FromRoute] Guid id, [FromBody] User users)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != users.UserId)
            {
                ModelState.AddModelError("", "Update error");
                return BadRequest(ModelState);
            }

            this._context.Entry(users).State = EntityState.Modified;

            try
            {
                await this._context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsersExists(id))
                {
                    ModelState.AddModelError("", "Update error");
                    return NotFound(ModelState);
                }
                else
                {
                    throw;
                }
            }
            catch(Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return BadRequest(ModelState);
            }

            return NoContent();
        }

        // POST: api/Users
        [HttpPost]
        [Route("newUser")]
        public async Task<IActionResult> Post([FromBody] User user)
        {
            var checkUserEmail = await this._context.Users?.FindAsync(user.UserEmail);
            if (checkUserEmail != null)
            {
                ModelState.AddModelError("", "Email not unique ");
            }
            if (user.UserName == "admin")
            {
                ModelState.AddModelError("Name", "Invalid username - admin");
            }
            // если есть лшибки - возвращаем ошибку 400
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (user.UserId == Guid.Empty)
            {
                user.UserId = Guid.NewGuid();
                user.UserPw = CONST.PWSTART;
            }

            this._context.Users.Add(user);
            try
            {
                await this._context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (UsersExists(user.UserId))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetUsers", new { id = user.UserId }, user);
        }

        // DELETE: api/Users/5
        [HttpDelete("{id}")]
       // [Authorize]
        public async Task<IActionResult> Delete([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var users = await this._context.Users.FindAsync(id);
            if (users == null)
            {
                ModelState.AddModelError("", "User not found");
                return NotFound(ModelState);
            }

            this._context.Users.Remove(users);
            await this._context.SaveChangesAsync();

            return Ok(users);
        }

        private bool UsersExists(Guid id)
        {
            return this._context.Users.Any(e => e.UserId == id);
        }
    }
}