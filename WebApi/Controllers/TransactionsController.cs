﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly PWApplicationContext _context;

        public TransactionsController(PWApplicationContext context)
        {
            this._context = context;
        }

        // GET: api/Transaction
        [HttpGet]
       // [Authorize]
        public IEnumerable<Transactions> Get()
        {
            return this._context.Transactions;
        }

        // GET: api/Transaction/5
        [HttpGet("{id}")]
       // [Authorize]
        public async Task<IActionResult> Get([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var transactions = await this._context.Transactions.FindAsync(id);

            if (transactions == null)
            {
                return NotFound();
            }

            return Ok(transactions);
        }

        // POST: api/Transaction
        [HttpPost]
        //[Authorize]
        public async Task<IActionResult> Post([FromBody] Transactions transactions)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var fromUser = this._context.Users?.Find(transactions.FromUser);
            var toUser = this._context.Users?.Find(transactions.ToUser);

            if (checkUserPW(transactions, fromUser))
            {
                ModelState.AddModelError("", "there is not enough money in the account");
                return BadRequest(ModelState);
            }
            using (var transaction = await this._context.Database.BeginTransactionAsync())
            {
                transferFunds(transactions, fromUser);
                reduceFunds(transactions, fromUser);
                this._context.Transactions.Add(transactions);
                try
                {
                    await this._context.SaveChangesAsync();
                    transaction.Commit();
                }
                catch (DbUpdateException)
                {
                    if (transactionsExists(transactions.TransactionId))
                    {
                        return new StatusCodeResult(StatusCodes.Status409Conflict);
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return CreatedAtAction("GetTransactions", new { id = transactions.TransactionId }, transactions);
        }

        private bool transactionsExists(Guid id)
        {
            return this._context.Transactions.Any(e => e.TransactionId == id);
        }

        private bool checkUserPW(Transactions transactions, User user)
        {
            return user.UserPw >= transactions.Pw ? true : false;
        }

        private void transferFunds(Transactions transactions, User user)
        {
            user.UserPw += transactions.Pw;
        }

        private void reduceFunds(Transactions transactions, User user)
        {
            user.UserPw -= transactions.Pw;
        }
    }
}