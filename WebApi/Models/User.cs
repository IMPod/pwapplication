﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models
{
    public partial class User
    {
        public User()
        {
            TransactionsFromUser = new HashSet<Transactions>();
            TransactionsToUser = new HashSet<Transactions>();
        }

        [Key]
        public Guid UserId { get; set; }

        [Required(ErrorMessage = "Please enter UserName")]
        [StringLength(50)]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        public string UserEmail { get; set; }

        [Required]
        public string UserPassword { get; set; }
        public int? UserPw { get; set; }

        public ICollection<Transactions> TransactionsFromUser { get; set; }
        public ICollection<Transactions> TransactionsToUser { get; set; }
    }
}
