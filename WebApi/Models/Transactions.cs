﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models
{
    public partial class Transactions
    {
        [Key]
        public Guid TransactionId { get; set; }
        public Guid FromUserId { get; set; }
        public Guid ToUserId { get; set; }
        public int? Pw { get; set; }
        public DateTime? TransactionTime { get; set; }

        public User FromUser { get; set; }
        public User ToUser { get; set; }
    }
}
